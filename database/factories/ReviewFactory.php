<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
       $faker = Faker\Factory::create();
        return [
            'product_id'=> function(){
                return Product::all()->random();
            },
            'customer'=> $faker->word,
            'review'=> $faker->paragraph,
            'star'=> $faker->numberBetween(0,5),
        ];
    }
}
