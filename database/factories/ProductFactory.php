<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = Faker\Factory::create();
        return [
            'name'=> $faker->word,
            'detail'=> $faker->paragraph,
            'price'=> $faker->numberBetween(100,1000),
            'discount'=> $faker->randomDigit,
            'stock'=> $faker->numberBetween(2,300),
        ];
    }
}
